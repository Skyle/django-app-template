# {{app_name}}

## Getting Started

Make sure you are using a virtual environment of some sort (e.g. `virtualenv` or
`pyenv`).


Installation
------------

Installation using pip:

    pip install mire_book or pip install -r requirements.txt

Add to installed apps:

    INSTALLED_APPS = [
        '{{app_name}}',
        ...
    ]

Usage
-----
run commande:

    django-admin.py run server

Browse to http://localhost:8000/
