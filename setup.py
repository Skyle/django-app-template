from io import open
from setuptools import find_packages, setup
import os
from pip.req import parse_requirements
from pip.download import PipSession
import {{app_name}}

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

README = open('README.md', encoding="utf-8").read()

base = os.path.dirname(os.path.abspath(__file__))

req_path = os.path.join(base,'requirements.txt')
install_reqs = parse_requirements(req_path, session=PipSession())

required = []
required_link= []

for ir in install_reqs:
    if ir.original_link:
        required_link.append(str(ir.original_link).split('+')[1] )
    if ir.req:
        required.append(str(ir.req))

install_requires = required
dependency_links = required_link

setup(
    name='{{app_name}}',
    version={{app_name}}.__version__,
    packages=find_packages(),
    include_package_data=True,
    license={{app_name}}.__license__,
    description='{{app_name}}',
    long_description=README,
    url={{app_name}}.__url__,
    author={{app_name}}.__author__,
    author_email={{app_name}}.__email__,
    keywords='django, server, runserver, {{app_name}}',
    classifiers=[
        'Framework :: Django',
        'Environment :: Web Environment',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: BSD License',
	'Topic :: Internet :: WWW/HTTP',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    install_requires=install_requires,
    dependency_links=dependency_links,
)
